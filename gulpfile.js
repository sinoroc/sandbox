//


!(function gulpfile() {
    'use strict';


    var DIST = './public/';
    var ICON_FILE_NAME = 'icon.png';
    var STATIC_FILES = [
        './src/static/**/*.html',
        './src/static/**/*.js',
        './src/static/**/*.json'
    ];


    var gulp = require('gulp');


    var buildBaseIcon = function buildBaseIcon(callback) {
        var SIZE = 1;
        var COLOR = 0xFF0000FF;
        var Jimp = require("jimp");
        var img = new Jimp(SIZE, SIZE, COLOR, function (err, image) {
            image.write(ICON_FILE_NAME, callback);
        });
    };


    var iconTask = function iconTask() {
        buildBaseIcon(function iconWriteCallback() {
            var favicons = require('gulp-favicons');
            var icon = ICON_FILE_NAME;
            var cfg = {
                'appDescription': "The Sandbox application",
                'appName': "Sandbox",
                'background': '#f00',
                'display': 'standalone',
                'html': DIST + 'index.html',
                'path': '.',
                'start_url': 'index.html'
            };
            return gulp.src(icon).pipe(favicons(cfg)).pipe(gulp.dest(DIST));
        })
    };


    var staticTask = function staticTask() {
        return gulp.src(STATIC_FILES).pipe(gulp.dest(DIST));
    };


    var cleanTask = function cleanTask(callback) {
        var del = require('del');
        del([ICON_FILE_NAME, DIST], callback);
    };


    var watchTask = function watchTask() {
        return gulp.watch(STATIC_FILES, ['default']);
    };


    var main = function main() {
        gulp.task('default', ['static', 'icon']);
        gulp.task('clean', cleanTask);
        gulp.task('icon', ['static'], iconTask);  // icon writes in index.html
        gulp.task('static', staticTask);
        gulp.task('watch', watchTask);
    };


    main();


}());


// EOF
