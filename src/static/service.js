//


!(function serviceModule(theWorker) {
    'use strict';


    var APP_CACHE = 'application-v0';
    var APP_FILES = [
        '.',
        'app.js',
        'favicon.ico',
        'index.html',
        'manifest.json',
        'service.js'
    ];
    var THINGS_CACHE = 'things-v0';


    var showNotification = function showNotification(worker, message) {
        var options = {
            'body': message,
            'icon': 'favicon.ico'
        };
        console.log("Show notification:", message);
        if (Notification.permission === 'granted') {
            worker.registration.showNotification("Notification", options);
        }
    };


    var onFetch = function onFetch(event) {
        var cache = true;
        var worker = this;  // "var worker = event.target;"?
        console.log("Fetching...", event.request.url);
        if (event.request.method !== 'GET') {
            cache = false;
        } else if (event.request.url.endsWith('things/index.json')) {
            cache = false;
        }
        if (cache) {
            event.respondWith(caches.match(event.request).then(function (maybeCached) {
                return queriedCache(worker, maybeCached, event.request);
            }));
        } else {
            console.log("Skip cache.");
            event.respondWith(fetch(event.request));
        }
    };


    var queriedCache = function queriedCache(worker, maybeCached, request) {
        var result = null;
        if (maybeCached) {
            console.log("Found in the cache");
            result = maybeCached;
        } else {
            console.log("Not found in the cache");
            result = fetch(request).then(function (response) {
                return addToCache(worker, response, request);
            });
        }
        return result;
    };


    var addToCache = function addToCache(worker, response, request) {
        var cloned = response.clone();
        var cacheName = null;
        if (request.url.indexOf('/things/') !== 0) {
            cacheName = THINGS_CACHE;
        } else {
            cacheName = APP_CACHE;
        }
        console.log("Caching", response.url, "in", cacheName, "...");
        caches.open(THINGS_CACHE).then(function (cache) {
            return cache.put(request, cloned);
        }).then(function () {
            showNotification(worker, "Cached:\n" + response.url);
        });
        return response;
    };


    var cacheApplication = function cacheApplication(worker, cache) {
        cache.addAll(APP_FILES).then(function (){
            console.log("Application cached.");
            showNotification(worker, "Application cached.");
        }).catch(function () {
            console.log("Could not cache application!");
        });
    };


    var onInstall = function onInstall(event) {
        var worker = this;
        console.log("Caching app...");
        event.waitUntil(caches.open(APP_CACHE).then(function cacheApp(cache) {
            cacheApplication(worker, cache);
        }));
    };


    var onActivate = function onActivate(event) {
        console.log("Service worker is now ready.");
    };


    var init = function init(worker) {
        worker.addEventListener('install', onInstall);
        worker.addEventListener('activate', onActivate);
        worker.addEventListener('fetch', onFetch);
    };


    var main = function main(worker) {
        init(worker);
    };


    main(theWorker);


}(this));



// EOF
