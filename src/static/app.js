//


!(function appModule() {
    'use strict';


    var SERVICE_WORKER_URL = 'service.js';
    var STATE_ON = '&#10003;';  // Check Mark
    var STATE_OFF = '&#10007;';  // Ballot X
    var THINGS_CACHE = 'things-v0';
    var THINGS_URL = 'things/index.json';


    var addItem = function addItem(url, element) {
        var item = document.createElement('li');
        var link = document.createElement('a');
        link.innerText = url;
        link.href = url;
        link.target = 'thing';
        item.appendChild(link);
        element.appendChild(item);
    };


    var clearItems = function clearItems(element) {
        while(element.lastChild) {
            element.removeChild(element.lastChild);
        }
    };


    var listCachedDocuments = function listCachedDocuments() {
        var offline = document.getElementById('offline');
        console.log("Listing cached documents...");
        clearItems(offline);
        caches.open(THINGS_CACHE).then(function matchAll(cache) {
            return cache.matchAll();
        }).then(function displayItems(items) {
            items.forEach(function (item) {
                addItem(item.url, offline);
            });
        });
    };


    var listOnlineThings = function listOnlineThings() {
        var online = document.getElementById('online');
        console.log("Listing online documents...");
        clearItems(online);
		fetch(THINGS_URL).then(function extractJson(response) {
            return response.json();
        }).then(function parseJson(json) {
            var things = json.result;
            things.forEach(function (thing) {
                var url = 'things/' + thing + '.html';
                addItem(url, online);
            });
        });
    };


    var loadClicked = function loadClicked(event) {
        listCachedDocuments();
        listOnlineThings();
    };


    var updateServiceWorkerIcon = function updateServiceWorkerIcon(state) {
        var value = STATE_OFF;
        if (state === 'activated') {
            value = STATE_ON;
        }
        document.getElementById('service-worker-toggle').innerHTML = value;
    };


    var updateServiceWorkerState = function updateServiceWorkerState(input) {
        var state = null;
        var worker = null;
        if (input === undefined) {
            navigator.serviceWorker.getRegistration().then(function (reg) {
                if (reg) {
                    updateServiceWorkerState(reg);
                } else {
                    updateServiceWorkerState(null);
                }
            });
        } else {
            if (input instanceof ServiceWorkerRegistration && input.active) {
                worker = input.active;
            } else if (input instanceof ServiceWorker) {
                worker = input;
            }
            if (worker) {
                state = worker.state;
            }
            console.log("Service worker state is:", state);
            updateServiceWorkerIcon(state);
        }
    };


    var startServiceWorkerMonitoring = function startServiceWorkerMonitoring(worker) {
        worker.addEventListener('statechange', function (event) {
            updateServiceWorkerState(event.target);
        });
        updateServiceWorkerState(worker);
    };


    var stopServiceWorkerMonitoring = function stopServiceWorkerMonitoring(worker) {
        worker.removeEventListener('statechange', updateServiceWorkerState);
    };


    var findServiceWorker = function findServiceWorker() {
        console.log("Looking for service worker...");
        navigator.serviceWorker.getRegistration().then(function (registration) {
            if (registration && registration.active) {
                console.log("Service worker found.");
                startServiceWorkerMonitoring(registration.active);
            } else {
                console.log("Service worker not found.");
                updateServiceWorkerState(null);
            }
        });
    };


    var success = function success(registration) {
        var worker = null;
        if (registration) {
            if (registration.scope) {
                console.log("Service worker registered on: ", registration.scope);
            }
            if (registration.installing) {
                worker = registration.installing;
            } else if (registration.waiting) {
                worker = registration.waiting;
            } else if (registration.active) {
                worker = registration.active;
            }
        }
        if (worker) {
            startServiceWorkerMonitoring(worker);
        }
    };


    var failure = function failure(err) {
        console.log("Service worker registration failed: ", err);
        updateServiceWorkerState();
    };


    var registerServiceWorker = function registerServiceWorker() {
        var url = SERVICE_WORKER_URL;
        console.log("Registering service worker...");
        navigator.serviceWorker.register(url).then(success).catch(failure);
    };


    var unregisterServiceWorker = function unregisterServiceWorker(registration) {
        var worker = registration.active;
        console.log("Unregistering service worker...");
        registration.unregister().then(function (result) {
            if (result === true) {
                navigator.serviceWorker.getRegistration().then(function (reg) {
                    if (reg) {
                        console.log("Could not unregister service worker!");
                        updateServiceWorkerState(reg);
                    } else {
                        console.log("Service worker unregistered.");
                        updateServiceWorkerState(null);
                        stopServiceWorkerMonitoring(worker);
                    }
                });
            } else {
                console.log("Could not unregister service worker!");
                updateServiceWorkerState(registration);
            }
        });
    };


    var toggleServiceWorker = function toggleServiceWorker() {
        console.log("<<< toggleServiceWorker >>>");
        navigator.serviceWorker.getRegistration().then(function (registration) {
            if (registration && registration.active) {
                unregisterServiceWorker(registration);
            } else {
                registerServiceWorker();
            }
        });
    };


    var initServiceWorkerToggle = function initServiceWorkerToggle() {
        var toggle = document.getElementById('service-worker-toggle');
        toggle.addEventListener('click', toggleServiceWorker);
        findServiceWorker();
    };


    var initServiceWorker = function initServiceWorker() {
        initServiceWorkerToggle();
    };


    var updateNotificationToggle = function updateNotificationToggle() {
        var toggle = document.getElementById('notification-toggle');
        if (Notification.permission === 'granted') {
            toggle.innerHTML = STATE_ON;
        } else {
            toggle.innerHTML = STATE_OFF;
        }
    };


    var askNotificationPermission = function askNotificationPermission(event) {
        Notification.requestPermission().then(updateNotificationToggle);
    };


    var initNotificationToggle = function initNotificationToggle() {
        var toggle = document.getElementById('notification-toggle');
        toggle.addEventListener('click', askNotificationPermission);
        updateNotificationToggle();
    };


    var init = function init() {
        initNotificationToggle();
        initServiceWorker();
        document.getElementById('load').addEventListener('click', loadClicked);
    };


    var main = function main() {
        document.addEventListener('DOMContentLoaded', init, false);
    };


    main();


}());


// EOF
